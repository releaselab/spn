# SPN - Scientific Publishing Network

This repository contains the decentralized application SPN.

All the source code is in the `source` folder.


## Demo

If you want to try it out, you could run the containerized DApp in the `spn_container` folder. To do this, follow the instructions bellow.

## Pre-requisites

You'll need the following software installed on your machine:

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Instructions

```
git clone https://gitlab.com/releaselab/spn.git
cd spn/spn_container
docker-compose up -d
```

It could take a couple of minutes to build all the containers, depending on your network connection. Once the containers are build, the terminal shows the following message:

```
Starting spn_db ... done
Starting spn_ui ... done
Starting spn_api ... done
```


Next, open the url http://localhost:3000 in your browser. You should see a window similar to Figure 1.

![Main menu SPN](images/menuLogin.png)
Figure 1: SPN main page.

If you do a search by title for the word "SPN" , you should see the articles described in Figure 2:

![Search SPN](images/searchSPN.png)
Figure 2: Search by title.

To stop the DApp, run:
```
docker-compose stop
```

There are three containers in use:

- `spn_ui` who is responsable for serving the DApp, this container has a nginx server running.

- `spn_api` has [Node.js](https://nodejs.org/en/) and [Express](https://expressjs.com/) running and is responsable for the communication between the DApp and the database.

- `spn_db` has an instance of [Apache CouchDB](http://couchdb.apache.org/) in it. This is where all the 'off-chain' data of the DApp is stored.

The database has some inicial values for testing purposes. <br> 
It has five users, who can be used for testing. This users are based on the five bootstrap users from [Granary](https://github.com/stove-labs/granary/tree/master).

The Orcid ID for them are:

- **User_1** : `0000-0001-1234-0001`
- **User_2** : `0000-0001-1234-0002`
- **User_3** : `0000-0001-1234-0003`
- **User_4** : `0000-0001-1234-0004`
- **User_5** : `0000-0001-1234-0005`

All have the same password for signing in: `spn2019`. <br>

If you want to test the DApp with the sandbox running, so you can add new articles, you will need to acess the secret keys for each user, they can be found on the folder `node_spn/database_init`.

For running the sandbox see the instructions on the repository [Granary](https://github.com/stove-labs/granary/tree/master).


## Authors

- **Vladimiro Vaz** - _Main author and maintainer_ - [vladvaz](https://gitlab.com/vladvaz)
- **Daniel Fernandes** - _Main author_ - [Daniel_Fernandes](https://gitlab.com/Daniel_Fernandes)
- **Simão Melo de Sousa** - [smdsousa](https://gitlab.com/smdsousa)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/releaselab/spn/blob/master/LICENSE) file for details

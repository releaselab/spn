const couch = require("./DB");
const fs = require("fs");

// insere novo documento
insertArticle = async (obj, file_pdf, id) => {
  const path = "./article_data/" + file_pdf;

  let data = fs.readFileSync(path);

  if (data) {
    const DB = couch.use("d_articles");

    try {
      const res = await DB.multipart.insert(
        obj,
        [{ name: file_pdf, data: data, content_type: "application/pdf" }],
        id
      );

      console.log("Insert article: ", res);

      let typeArticle;
      if (obj.reviewed_article_id === "") {
        if (obj.previous_version === "") {
          typeArticle = "articles";
        } else {
          typeArticle = "versions";
          await updateNextVersionInArticle(obj.previous_version, id);
        }
      } else {
        typeArticle = "reviews";
        await updateReviewArrayOfArticle(
          obj.reviewed_article_id,
          obj.review_score,
          obj.expertise_score,
          id
        );
      }

      for (const author of obj.authors) {
        await updateArticleInAuthor(author.id, id, typeArticle);
      }
    } catch (error) {
      console.log("ERRO: " + error.message);
    }
  } else {
    console.log("Read file error");
  }
};

// update article in user's docs
updateArticleInAuthor = async (author, articleId, typeArticle) => {
  const DB = couch.use("d_users");

  try {
    let DB_doc = await DB.get(author);

    if (typeArticle === "articles" || typeArticle === "versions") {
      let newArray = DB_doc.articles;
      newArray.push(articleId);
      DB_doc.articles = newArray;
    }

    if (typeArticle === "reviews") {
      let newArray = DB_doc.reviews;
      newArray.push(articleId);
      DB_doc.reviews = newArray;
    }
    const doc = await DB.insert(DB_doc);
    console.log("Updated author: ", doc);
  } catch (err) {
    console.log("Updated author error: ", err.message);
  }
};

// update array of reviews in article
updateReviewArrayOfArticle = async (articleId, rate, expertise, IdRevision) => {
  const DB = couch.use("d_articles");

  try {
    let DB_doc = await DB.get(articleId);
    let newArray = DB_doc.reviews;
    newArray.push({ id: IdRevision, score: rate, expertise: expertise });
    DB_doc.reviews = newArray;
    const doc = await DB.insert(DB_doc);
    console.log("Updated review: ", doc);
  } catch (err) {
    console.log("Updated review error: ", err.message);
  }
};

// update previous version in article
updateNextVersionInArticle = async (articleId, versionId) => {
  const DB = couch.use("d_articles");

  try {
    let DB_doc = await DB.get(articleId);
    DB_doc.next_version = versionId;
    const doc = await DB.insert(DB_doc);
    console.log("Updated version: ", doc);
  } catch (err) {
    console.log("Updated version error: ", err.message);
  }
};

async function inicia(array) {
  for (const index of array) {
    console.log("index: ", index);

    let filename = `./article_data/art_${index}.json`;
    let file_pdf = `art_${index}.pdf`;
    let rawdata = fs.readFileSync(filename);
    let obj = JSON.parse(rawdata);
    //console.log(obj.title);
    await insertArticle(obj, file_pdf, index.toString());
    setTimeout(() => {}, 1000);
  }
}

inicia([1, 2, 3]);

// DB.js
const config = require("config");
const nano = require("nano");

const user = "admin";
const password = config.get("couchPrivateKey");
const ip = "database";
const port = "5984";

const couch = nano(`http://${user}:${password}@${ip}:${port}`);

module.exports = couch;

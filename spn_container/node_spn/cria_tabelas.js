const couch = require("./DB");

criaTabela = async tabela => {
  try {
    await couch.db.create(tabela);
  } catch (error) {
    console.log(tabela + " -- " + error.message);
  }
};

apagaTabela = async tabela => {
  try {
    await couch.db.destroy(tabela);
  } catch (error) {
    console.log(tabela + " -- " + error.message);
  }
};

addAuthors = async () => {
  try {
    const obj = {
      all_authors: []
    };
    const id = await couch.uuids(1);
    const DB = couch.use("d_authors");
    const res_obj = await DB.insert(obj, id.uuids[0]);
    console.log(res_obj);
  } catch (error) {
    console.log(d_authors + " -- " + error.message);
  }
};

apaga = async () => {
  await apagaTabela("d_articles");
  await apagaTabela("d_authors");
  await apagaTabela("d_comments");
  await apagaTabela("d_users");
  cria();
};

cria = async () => {
  await criaTabela("d_articles");
  await criaTabela("d_authors");
  await criaTabela("d_comments");
  await criaTabela("d_users");
  await addAuthors();
};

apaga();

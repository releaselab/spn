const bcrypt = require("bcrypt");
const couch = require("./DB");

preencheDados = async user => {
  try {
    const hashed = await geraPalavraChave(user.password);
    user.password = hashed;
    // console.log(hashed);

    const db = couch.use("d_users");
    const res_obj = await db.insert(user, user._id);
    console.log("RES: ", res_obj);
    const updRes = await actualizaTabelaAuthors(user);
    console.log(updRes);
  } catch (error) {
    console.log("ERR: ", error.message);
  }
};

geraPalavraChave = async palavra => {
  const salt = await bcrypt.genSalt(10);
  const hashed = await bcrypt.hash(palavra, salt);
  return hashed;
};

actualizaTabelaAuthors = async user => {
  let novoAutor = {
    name: user.name,
    id: user._id,
    pkh: user.pkh
  };

  const db = couch.use("d_authors");
  try {
    const docs = await db.list({ include_docs: true });

    let doc = docs.rows[0].doc;
    doc.all_authors.push(novoAutor);

    const res_db = await db.insert(doc);
    return res_db;
    console.log(res_db);
  } catch (err) {
    console.log(err.message);
    return err.message;
  }
};

inicia = async () => {
  for (let index = 1; index < 6; index++) {
    const fs = require("fs");
    let filename = `./database_init/user${index}.json`;
    let rawdata = fs.readFileSync(filename);
    let student = JSON.parse(rawdata);
    // console.log(student);
    await preencheDados(student);
  }
};

inicia();

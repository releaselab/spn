// article route
const auth = require("../middleware/auth");
const couch = require("../DB");
const express = require("express");
const articlesRoutes = express.Router();

// get document by ID
articlesRoutes.get("/docs/:id", (req, res) => {
  const DB = couch.use("d_articles");
  DB.get(req.params.id)
    .then(body => {
      res.send(body);
    })
    .catch(err => {
      res.sendStatus(err.statusCode);
    });
});

// get multi docs by ids
articlesRoutes.post("/docs/multi", async (req, res) => {
  const DB = couch.use("d_articles");
  console.log(req.body);

  try {
    const docs = await DB.fetch({ keys: req.body.articles });
    res.send(docs);
  } catch (err) {
    console.log(err.message);
  }
});

// get documents by query
articlesRoutes.get("/search", (req, res) => {
  const obj = req.query;
  const key = Object.keys(obj)[0];

  if (key === "title") searchByTitle(req.query.title, res);
  if (key === "author") searchByAuthor(req.query.author, res);
  if (key === "tag") searchByTag(req.query.tag, res);
});

// add new document
articlesRoutes.post("/add", auth, (req, res) => {
  insertArticle(req.body, res);
});

// AUX FUNCTIONS
//get new uuids
getUUIDS = async () => {
  try {
    const res = await couch.uuids(1);
    console.log(res);
    return res.uuids.toString;
  } catch (error) {
    console.log(error.message);
  }
};

// insere novo documento
insertArticle = async (obj, res) => {
  try {
    const obj_desc = {
      title: obj.title,
      authors: obj.authors,
      tags: obj.tags,
      article_abstract: obj.article_abstract,
      article_version: obj.article_version,
      next_version: obj.next_version,
      previous_version: obj.previous_version,
      reviewed_article_id: obj.reviewed_article_id,
      reviews: obj.reviews,
      comments: obj.comments,
      positive_rates: obj.positive_rates,
      negative_rates: obj.negative_rates,
      submitter_pkh: obj.submitter_pkh,
      hash_contract: obj.hash_contract,
      date: obj.date,
      review_score: obj.review_score,
      expertise_score: obj.expertise_score
    };

    const id = await couch.uuids(1);
    const DB = couch.use("d_articles");
    const res_obj = await DB.insert(obj_desc, id.uuids[0]);

    const res_atta = await DB.attachment.insert(
      id.uuids[0],
      obj.file_name,
      Buffer.from(obj.article_buffer.data),
      obj.file_type,
      {
        rev: res_obj.rev
      }
    );

    let typeArticle;
    if (obj.reviewed_article_id === "") {
      if (obj.previous_version === "") {
        typeArticle = "articles";
      } else {
        typeArticle = "versions";
        updateNextVersionInArticle(obj.previous_version, id.uuids[0]);
      }
    } else {
      typeArticle = "reviews";
      updateReviewArrayOfArticle(
        obj.reviewed_article_id,
        obj.review_score,
        obj.expertise_score,
        id.uuids[0]
      );
    }

    obj.authors.map(author => {
      updateArticleInAuthor(author.id, id.uuids[0], typeArticle);
    });

    res.sendStatus(200);
  } catch (error) {
    console.error(error);
    res.sendStatus(400);
  }
};

// update article in user's docs
updateArticleInAuthor = async (author, articleId, typeArticle) => {
  const DB = couch.use("d_users");

  try {
    let DB_doc = await DB.get(author);

    if (typeArticle === "articles" || typeArticle === "versions") {
      let newArray = DB_doc.articles;
      newArray.push(articleId);
      DB_doc.articles = newArray;
    }

    if (typeArticle === "reviews") {
      let newArray = DB_doc.reviews;
      newArray.push(articleId);
      DB_doc.reviews = newArray;
    }
    const doc = await DB.insert(DB_doc);
    console.log(doc);
  } catch (err) {
    console.log(err.message);
  }
};

// update array of reviews in article
updateReviewArrayOfArticle = async (articleId, rate, expertise, IdRevision) => {
  const DB = couch.use("d_articles");

  try {
    let DB_doc = await DB.get(articleId);
    let newArray = DB_doc.reviews;
    newArray.push({ id: IdRevision, score: rate, expertise: expertise });
    DB_doc.reviews = newArray;
    const doc = await DB.insert(DB_doc);
    console.log(doc);
  } catch (err) {
    console.log(err.message);
  }
};

// update previous version in article
updateNextVersionInArticle = async (articleId, versionId) => {
  const DB = couch.use("d_articles");

  try {
    let DB_doc = await DB.get(articleId);
    DB_doc.next_version = versionId;
    const doc = await DB.insert(DB_doc);
    console.log(doc);
  } catch (err) {
    console.log(err.message);
  }
};

searchByTitle = async (params, res) => {
  let arr = [];
  const obj = JSON.parse(params);
  obj.keywords.map(word => {
    let s = `(?i)${word}`;
    let rule = { title: { $regex: s } };
    arr.push(rule);
  });

  const q = {
    selector: { $or: arr }
  };

  try {
    const DB = couch.use("d_articles");
    const response = await DB.find(q);
    res.send(response);
  } catch (error) {
    res.sendStatus(error.statusCode);
  }
};

// search by author
searchByAuthor = async (params, res) => {
  let arr = [];
  const obj = JSON.parse(params);
  obj.keywords.map(word => {
    let s = `(?i)${word}`;
    let rule = { authors: { $elemMatch: { name: { $regex: s } } } };
    arr.push(rule);
  });

  const q = {
    selector: { $or: arr }
  };

  try {
    const DB = couch.use("d_articles");
    const response = await DB.find(q);
    res.send(response);
  } catch (error) {
    res.sendStatus(error.statusCode);
  }
};

// search by tag
searchByTag = async (params, res) => {
  let arr = [];
  const obj = JSON.parse(params);
  obj.keywords.map(word => {
    let s = `(?i)${word}`;
    let rule = { tags: { $elemMatch: { $regex: s } } };
    arr.push(rule);
  });

  const q = {
    selector: { $or: arr }
  };

  try {
    const DB = couch.use("d_articles");
    const response = await DB.find(q);
    res.send(response);
  } catch (error) {
    res.sendStatus(error.statusCode);
  }
};

module.exports = articlesRoutes;

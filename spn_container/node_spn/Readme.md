# **API da SPN - Scientific Publishing Network**

Esta API foi criada utilizando a _framework_ [Express](https://expressjs.com/).

## Instruções

É necessário ter a [Apache CouchDB](http://couchdb.apache.org/) instalada e configurada com `username` e `password`. Para configurar a base de dados abrir no navegador o seguinte endereço:

http://localhost:5984/_utils/#login

É necessário criar duas variáveis de ambiente com as palavras-chave da base de dados e da criação dos [JWT](https://jwt.io/).

### Criar variáveis de ambiente

```
export spn_jwtPrivateKey=<password_jwt>
export spn_couchPrivateKey=<password_couchdb>
```

### Correr API

```
git clone https://gitlab.com/releaselab/spn_api.git
cd spn_api
npm install
node server.js
```

A API recebe pedidos por omissão no endereço: `http://localhost:4000`.

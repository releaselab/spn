# SPN UI

This project was created with the _boilerplate_ code [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites

- [Node.js](https://nodejs.org/en/)
- [Npm](https://www.npmjs.com/)


You'll need to have the `API` running in the background (see api folder in this repository) and a [Granary](https://github.com/stove-labs/granary/tree/master) sandbox  node on `http://localhost:18731`, so you can add new files to the system. 

**Note:** To run a sandbox node see the instructions in the [Granary](https://github.com/stove-labs/granary/tree/master) repository.

## Instructions

```
cd ui
npm install
npm start
```

Open your browser on [http://localhost:3000](http://localhost:3000). We recomend using [Mozilla Firefox](https://www.mozilla.org/pt-PT/firefox/).<br> 
If you get `CORS` warnings, use the add-on `CORS Everywhere`. This is due to [Granary](https://github.com/stove-labs/granary/tree/master) not having CORS headers in the config files.

To modify the code, use and IDE (eg: Visula Studio Code).

## If you want to build a optimized build version use:

```
npm run build
```

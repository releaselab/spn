import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";
import { connect } from "react-redux";
import Alert from "react-bootstrap/Alert";
import url from "./ServerIP";

let styleMargin = {
  marginTop: "10%",
  marginLeft: "40%",
  marginRight: "40%"
};

class Login extends Component {
  state = {
    _id: "",
    password: "",
    validated: false,
    alert: false
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = async event => {
    event.preventDefault();
    event.stopPropagation();

    const form = event.currentTarget;
    if (form.checkValidity() === true) {
      await this.log();
    }
    this.setState({ validated: true });
  };

  handleDismiss = () => {
    this.setState({
      alert: false
    });
  };

  showAlert = () => {
    if (this.state.alert === true) {
      return (
        <div>
          <p />
          <Alert variant="danger" onClose={this.handleDismiss} dismissible>
            Orcid ID or password invalid!
          </Alert>
        </div>
      );
    }
  };

  log = () => {
    axios
      .post(url + "/users/auth", {
        _id: this.state._id,
        password: this.state.password
      })
      .then(res => {
        console.log(res.data);
        sessionStorage.setItem("_id", res.data._id);
        sessionStorage.setItem("name", res.data.name);
        sessionStorage.setItem("pkh", res.data.pkh);
        sessionStorage.setItem("spn_token", res.data.spn_token);
        sessionStorage.setItem("isLogged", true);
        this.props.updateState({ isLogged: true });
      })
      .catch(err => {
        console.log(err.message);
        this.setState({
          alert: true
        });
      });
  };

  render() {
    return (
      <div>
        <Form
          noValidate
          validated={this.state.validated}
          onSubmit={this.handleSubmit}
          style={styleMargin}
        >
          <Form.Group controlId="_id">
            <Form.Label>Orcid ID</Form.Label>
            <Form.Control
              type="text"
              name="_id"
              onChange={this.handleChange}
              required
            />
            <Form.Control.Feedback />
            <Form.Control.Feedback type="invalid">
              Input a Orcid ID!
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              onChange={this.handleChange}
              required
            />
            <Form.Control.Feedback />
            <Form.Control.Feedback type="invalid">
              Input a password!
            </Form.Control.Feedback>
          </Form.Group>

          <Button variant="info" type="submit">
            Login
          </Button>
          {this.showAlert()}
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogged: state.isLogged
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateState: new_state => {
      dispatch({ type: "UPDATE_STATE", new_state: new_state });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

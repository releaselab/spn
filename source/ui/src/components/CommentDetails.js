import React, { Component } from "react";
import Badge from "react-bootstrap/Badge";
import { Link } from "react-router-dom";
import axios from "axios";
import url from "./ServerIP";

let badgeMargin = {
  marginRight: "4px"
};

class CommentDetails extends Component {
  state = {
    _id: "",
    title: "",
    author: "",
    content: "",
    date: ""
  };

  getData() {
    let id = this.props.id;

    axios
      .get(url + "/comments/" + id)
      .then(response => {
        this.setState({
          _id: response.data._id,
          title: response.data.title,
          author: response.data.author,
          content: response.data.content,
          date: response.data.date
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.getData();
    }
  }

  render() {
    return (
      <div>
        <Link to={"/user/" + this.state.author.id}>
          <Badge variant="secondary" style={badgeMargin}>
            {this.state.author.name}
          </Badge>
        </Link>

        {this.state.date}
        <p style={{ marginBottom: "0.5rem" }} />
        {this.state.content}
        <p />
      </div>
    );
  }
}

export default CommentDetails;

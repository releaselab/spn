import React, { Component } from "react";
import HeaderBar from "./HeaderBar";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Typeahead } from "react-bootstrap-typeahead";
import axios from "axios";
import Tags from "./Tags";
import AppModals from "./AppModals";
import ReactDOM from "react-dom";
import url from "./ServerIP";

let styleLabel = {
  color: "Black"
};

class AddArticle extends Component {
  state = {
    title: null,
    authors: [],
    tags: [],
    article_abstract: "",
    hash_contract: "",
    sk: "",
    date: "",
    file_name: null,
    file_type: null,
    article_buffer: null,
    article_tags: "",
    options: [],
    validated: false,
    message: "",
    show: false,
    file_hash: ""
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = message => {
    this.setState({ message: message, show: true });
  };

  componentDidMount() {
    const headers = {
      spn_token: sessionStorage.getItem("spn_token")
    };

    axios
      .get(url + "/authors/", { headers })
      .then(response => {
        this.setState({ options: response.data.all_authors });
        console.log(response.data.all_authors);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  updateAuthors = e => {
    let novoArray = [...this.state.authors];
    novoArray[e.target.id] = e.target.value;
    this.setState({
      authors: novoArray
    });
  };

  handleAuthors = e => {
    if (e) {
      this.setState({
        authors: e
      });
    }
  };

  handleFile = e => {
    const file = e.target.files[0];
    let reader = new window.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => {
      const buff = Buffer.from(reader.result);

      this.setState({
        file_name: file.name,
        file_type: file.type,
        article_buffer: buff
      });
      console.log("FILE:", buff);
    };
  };

  upHashBlockChain = () => {
    const eztz = window.eztz;
    eztz.node.setProvider("http://localhost:18731");
    const sk = eztz.crypto.extractKeys(this.state.sk);
    let pkh_authors = this.state.authors.map(
      author => author.id + " : " + author.pkh
    );

    const pair =
      '(Pair "' +
      "FILE_SHA256: " +
      this.state.file_hash +
      '" "' +
      pkh_authors.join(" | ") +
      '")';

    const michel =
      "parameter unit;" +
      "storage (pair string string);" +
      "code { CDR  ;  NIL operation ; PAIR };";

    eztz.contract
      .originate(
        sk,
        0,
        michel,
        pair,
        false,
        false,
        null,
        "010000",
        50000,
        10000
      )
      .then(res => {
        console.log("HASH CONTRACT:", eztz.contract.hash(res.hash, 0));
        this.setState(
          {
            hash_contract: eztz.contract.hash(res.hash, 0)
          },
          () => {
            this.addFileToCouch();
          }
        );
      })
      .catch(err => {
        console.log(err);
        this.handleShow(
          "Communication with Tezos Blockchain failed. Please try again."
        );
      });
  };

  addFileToCouch = () => {
    const obj = {
      title: this.state.title,
      authors: this.state.authors,
      tags: this.state.tags,
      article_abstract: this.state.article_abstract,
      article_version: "1",
      next_version: "",
      previous_version: "",
      reviewed_article_id: "",
      reviews: [],
      comments: [],
      submitter_pkh: sessionStorage.getItem("pkh"),
      hash_contract: this.state.hash_contract,
      date: this.state.date,
      file_name: this.state.file_name,
      file_type: this.state.file_type,
      article_buffer: this.state.article_buffer,
      review_score: "",
      expertise_score: ""
    };
    console.log(obj);

    const headers = {
      spn_token: sessionStorage.getItem("spn_token")
    };


    axios
      .post(url + "/articles/add", obj, { headers })
      .then(res => {
        console.log(res.data);
        this.handleShow("Article Saved.");
        ReactDOM.findDOMNode(this.messageForm).reset();
        this.setState({ validated: false });
      })
      .catch(err => {
        console.log(err.message);
        this.handleShow("Problem during communication. Please try again.");
      });
  };

  eraseTag = tag => {
    const arr = [...this.state.tags];
    arr.splice(arr.indexOf(tag.target.id), 1);
    this.setState({
      tags: arr
    });
  };

  keyPressed = e => {
    if (e.key === "Enter") {
      this.verifyTags(this.state.article_tags);
    }
  };

  getDate = () => {
    const tempDate = new Date();
    const date =
      tempDate.getFullYear() +
      "-" +
      (tempDate.getMonth() + 1) +
      "-" +
      tempDate.getDate() +
      " " +
      tempDate.getHours() +
      ":" +
      tempDate.getMinutes() +
      ":" +
      tempDate.getSeconds();
    this.setState({
      date: date
    });
  };

  generateFileHash = () => {
    let crypto = require("crypto");
    let shasum = crypto.createHash("sha256");
    shasum.update(this.state.article_buffer);
    let file_hash = shasum.digest("hex");
    this.setState({
      file_hash: file_hash
    });
  };

  verifyUserAsAuthor = () => {
    let storeId = sessionStorage.getItem("_id");
    let res = this.state.authors.find(o => {
      return o.id === storeId;
    });
    if (res) return true;
    else return false;
  };

  handleSubmit = async e => {
    e.preventDefault();
    e.stopPropagation();

    const form = e.currentTarget.form;
    console.log(form);

    if (
      form.checkValidity() === true &&
      this.state.authors.length > 0 &&
      this.verifyUserAsAuthor()
    ) {
      await this.getDate();
      await this.generateFileHash();
      await this.upHashBlockChain();
      //this.addFileToCouch();

      return;
    }
    this.setState({ validated: true });
  };

  verifyTags = newtag => {
    const exists = this.state.tags.includes(newtag);
    const arrayLength = this.state.tags.length;
    if (!exists && arrayLength < 10) {
      const newArray = [...this.state.tags, newtag];
      this.setState({
        tags: newArray,
        article_tags: ""
      });
    }
  };

  render() {
    let warning_authors = null;
    if (this.state.validated === true) {
      if (this.state.authors.length > 0) {
        let storeId = sessionStorage.getItem("_id");
        let res = this.state.authors.find(o => {
          return o.id === storeId;
        });
        if (res) {
          warning_authors = (
            <small>
              <span style={{ color: "#28a745" }}>Author looks good!</span>
            </small>
          );
        } else {
          warning_authors = (
            <small>
              <span style={{ color: "#dc3545" }}>
                User must be one of the authors!
              </span>
            </small>
          );
        }
      } else {
        warning_authors = (
          <small>
            <span style={{ color: "#dc3545" }}>Input an author!</span>
          </small>
        );
      }
    }

    return (
      <div>
        <HeaderBar header={"New Article"} />

        <AppModals
          message={this.state.message}
          show={this.state.show}
          handleShow={this.handleShow}
          handleClose={this.handleClose}
        />

        <Form
          noValidate
          validated={this.state.validated}
          id="myForm"
          ref={form => (this.messageForm = form)}
        >
          <Form.Group controlId="title">
            <Form.Label style={styleLabel}>Title</Form.Label>
            <Form.Control
              type="text"
              name="title"
              placeholder="Add a title..."
              onChange={this.handleChange}
              required
            />
            <Form.Control.Feedback type="invalid">
              Input a title!
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group controlId="author">
            <Form.Label style={styleLabel}>Authors</Form.Label>
            <Typeahead
              id="author"
              labelKey="name"
              multiple={true}
              options={this.state.options}
              onChange={this.handleAuthors}
              placeholder="Choose an author..."
              renderMenuItemChildren={option => (
                <div>
                  {option.name} -{" "}
                  <small>
                    <em>{option.id}</em>
                  </small>
                </div>
              )}
            />
            {warning_authors}
          </Form.Group>

          <Form.Group controlId="articlePath">
            <Form.Label style={styleLabel}>File</Form.Label>
            <Form.Control
              type="file"
              name="myFile"
              onChange={this.handleFile}
              required
            />
            <Form.Control.Feedback type="invalid">
              Input a file!
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group controlId="sk">
            <Form.Label style={styleLabel}>Tezos secret key</Form.Label>
            <Form.Control
              type="password"
              name="sk"
              placeholder="Add your Tezos secret key..."
              onChange={this.handleChange}
              required
            />
            <Form.Control.Feedback type="invalid">
              Input your Tezos secret key!
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group controlId="article_abstract">
            <Form.Label style={styleLabel}>Abstract</Form.Label>
            <Form.Control
              as="textarea"
              name="article_abstract"
              rows="5"
              onChange={this.handleChange}
              placeholder="Write an abstract..."
            />
          </Form.Group>

          <Form.Group controlId="article_tags">
            <Form.Label style={styleLabel}>Keywords</Form.Label>
            <Form.Control
              type="text"
              placeholder="Add a keyword..."
              onChange={this.handleChange}
              onKeyDown={this.keyPressed}
              value={this.state.article_tags}
            />
            <Tags tags={this.state.tags} eraseTag={this.eraseTag} />
          </Form.Group>

          <Button variant="info" onClick={this.handleSubmit}>
            Upload
          </Button>
        </Form>
      </div>
    );
  }
}

export default AddArticle;

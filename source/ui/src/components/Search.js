import React, { Component } from "react";
import HeaderBar from "./HeaderBar";
import ListFilesTemplate from "./ListFilesTemplate";
import ListGroup from "react-bootstrap/ListGroup";
import axios from "axios";
import url from "./ServerIP";
import Alert from "react-bootstrap/Alert";

class Search extends Component {
  state = {
    response: [],
    alert: false
  };

  searchDB = () => {
    axios
      .get(url + "/articles/search?" + this.props.match.params.params)
      .then(res => {
        if (res.data.docs.length === 0) this.setState({ alert: true });
        this.setState({
          response: res.data.docs
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  componentDidMount() {
    this.setState({ alert: false });
    this.searchDB();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.params !== prevProps.match.params.params) {
      this.setState({ alert: false });
      this.searchDB();
    }
  }

  handleDismiss = () => {
    this.setState({
      alert: false
    });
  };

  showAlert = () => {
    if (this.state.alert === true) {
      return (
        <div>
          <p />
          <Alert variant="warning" onClose={this.handleDismiss} dismissible>
            No articles found.
          </Alert>
        </div>
      );
    }
  };

  render() {
    return (
      <div>
        <HeaderBar header={"Search Results"} />
        {this.showAlert()}
        <ListGroup>
          {this.state.response.map(doc => (
            <ListFilesTemplate
              key={doc._id}
              id={doc._id}
              title={doc.title}
              authors={doc.authors}
              tags={doc.tags}
              reviews={doc.reviews}
            />
          ))}
        </ListGroup>
      </div>
    );
  }
}

export default Search;

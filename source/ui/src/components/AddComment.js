import React, { Component } from "react";
import HeaderBar from "./HeaderBar";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import axios from "axios";
import AppModals from "./AppModals";
import ReactDOM from "react-dom";
import url from "./ServerIP";

let styleLabel = {
  color: "Black"
};

class AddComment extends Component {
  state = {
    title: "",
    author: { name: "", id: "" },
    content: "",
    date: "",
    message: "",
    show: false
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = message => {
    this.setState({ message: message, show: true });
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  addFileToCouch = () => {
    const obj = {
      article_id: this.props.location.state.article_id,
      title: this.state.title,
      author: this.state.author,
      content: this.state.content,
      date: this.state.date
    };
    console.log(obj);

    const headers = {
      spn_token: sessionStorage.getItem("spn_token")
    };

    axios
      .post(url + "/comments/add", obj, { headers })
      .then(res => {
        console.log(res.data);
        this.handleShow("Comment Saved.");
        ReactDOM.findDOMNode(this.messageForm).reset();
        this.setState({ validated: false });
      })
      .catch(err => {
        console.log(err.message);
        this.handleShow("Problem during communication. Please try again.");
      });
  };

  getDate = () => {
    const tempDate = new Date();
    const date =
      tempDate.getFullYear() +
      "-" +
      (tempDate.getMonth() + 1) +
      "-" +
      tempDate.getDate() +
      " " +
      tempDate.getHours() +
      ":" +
      tempDate.getMinutes() +
      ":" +
      tempDate.getSeconds();
    this.setState({
      date: date
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    e.stopPropagation();

    const form = e.currentTarget.form;
    console.log(form);

    if (form.checkValidity() === true) {
      await this.getDate();
      await this.addFileToCouch();
      return;
    }
    this.setState({ validated: true });
  };

  componentDidMount() {
    const author = sessionStorage.getItem("name");
    const id = sessionStorage.getItem("_id");
    this.setState({
      author: { name: author, id: id }
    });
  }
  render() {
    return (
      <div>
        <HeaderBar header={"New Comment"} />

        <AppModals
          message={this.state.message}
          show={this.state.show}
          handleShow={this.handleShow}
          handleClose={this.handleClose}
        />

        <Form
          noValidate
          validated={this.state.validated}
          id="myForm"
          ref={form => (this.messageForm = form)}
        >
          <Form.Group>
            <Form.Label style={styleLabel}>Author</Form.Label>
            <Form.Control type="text" readOnly value={this.state.author.name} />
          </Form.Group>

          <Form.Group controlId="content">
            <Form.Label style={styleLabel}>Content</Form.Label>
            <Form.Control
              as="textarea"
              rows="5"
              onChange={this.handleChange}
              placeholder="Write a comment..."
              required
            />
            <Form.Control.Feedback>Comment looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">
              Input a comment!
            </Form.Control.Feedback>
          </Form.Group>

          <Button variant="info" onClick={this.handleSubmit}>
            Upload
          </Button>
        </Form>
      </div>
    );
  }
}

export default AddComment;

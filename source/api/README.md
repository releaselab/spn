# **API da SPN - Scientific Publishing Network**

This API uses an [Express](https://expressjs.com/) _framework_.

## Prerequisites

- [Node.js](https://nodejs.org/en/)
- [Express](https://expressjs.com/)

You must have [Apache CouchDB](http://couchdb.apache.org/) installed and configured with a `username` and a `password`.

Use the follow url in a browser to configure the database.

http://localhost:5984/_utils/#login

## Instructions

You'll need two environment variables with the passwords to the database and to the [JWT](https://jwt.io/) token creation.


### Define the environment variables

```
export spn_jwtPrivateKey=<password_jwt>
export spn_couchPrivateKey=<password_couchdb>
```

### Run the API

```
cd api
npm install
node server.js
```

The API listens at the address: `http://localhost:4000`.

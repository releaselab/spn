const jwt = require("jsonwebtoken");
const config = require("config");

auth = (req, res, next) => {
  const token = req.header("spn_token");
  if (!token) return res.status(401).send("Access denied. No token provided.");

  try {
    const decode = jwt.verify(token, config.get("jwtPrivateKey"));
    console.log(decode);

    next();
  } catch (err) {
    res.status(400).send("Invalid token");
  }
};

module.exports = auth;

// comments route
const auth = require("../middleware/auth");
const couch = require("../DB");
const express = require("express");
const commentsRoutes = express.Router();

// get document by ID
commentsRoutes.get("/:id", (req, res) => {
  const DB = couch.use("d_comments");
  DB.get(req.params.id)
    .then(body => {
      res.send(body);
    })
    .catch(err => {
      res.sendStatus(err.statusCode);
    });
});

// add comment to DB
commentsRoutes.post("/add", auth, async (req, res) => {
  try {
    const id = await couch.uuids(1);
    updateCommentsInArticle(req.body.article_id, id.uuids[0]);
    const DB = couch.use("d_comments");
    const res_obj = await DB.insert(req.body, id.uuids[0]);
    res.send(res_obj);
  } catch (err) {
    res.send(err.message);
  }
});

// Auxiliary Functions
updateCommentsInArticle = async (articleId, commentId) => {
  try {
    const DB = couch.use("d_articles");
    let DB_doc = await DB.get(articleId);
    let newArray = DB_doc.comments;
    newArray.push(commentId);
    DB_doc.comments = newArray;
    const doc = await DB.insert(DB_doc);
    console.log(doc);
  } catch (err) {
    console.log(err.message);
  }
};

module.exports = commentsRoutes;

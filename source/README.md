# Source code to SPN DApp

This directory has two folders, the `ui (user interface)` folder and the `api (application programming interface)` folder. Use them if you want to see how the DApp was build, or if you want to make changes to it.

Each folder has instructions on how to run each component of this DApp.